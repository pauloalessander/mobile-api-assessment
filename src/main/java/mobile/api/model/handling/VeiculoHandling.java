package mobile.api.model.handling;

import java.io.Serializable;
import java.time.LocalDate;

import mobile.api.model.FuelEnum;

public class VeiculoHandling implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private LocalDate data;
	private String hora;
	private Integer odometro;
	private FuelEnum combustivel;
	private Double preco;
	private Double valor;
	private Boolean tanque;
	
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public Integer getOdometro() {
		return odometro;
	}
	public void setOdometro(Integer odometro) {
		this.odometro = odometro;
	}
	public FuelEnum getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(FuelEnum combustivel) {
		this.combustivel = combustivel;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Boolean getTanque() {
		return tanque;
	}
	public void setTanque(Boolean tanque) {
		this.tanque = tanque;
	}
}
