package mobile.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FuelEnum {
	GASOLINA("Gasolina"), ETANOL("Etanol"), DIESEL("Diesel"), GNV("GNV");

	private String value;

	FuelEnum(String value) {
        this.value = value;
    }
	
    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static FuelEnum of(String value) throws Exception {
        if (null == value) {
            return null;
        }

        for (FuelEnum item : FuelEnum.values()) {
            if (value.equals(item.getValue())) {
                return item;
            }
        }

        throw new Exception("FuelEnum: unknown value: " + value);
    }
}
