package mobile.api.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "veiculo")
public class Veiculo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private LocalDate data;
	
	@Column(length = 8)
	private String hora;
	private Integer odometro;
	private Integer kmRodado;
	
	@Column(length = 50)
	@Enumerated(EnumType.STRING)
	private FuelEnum combustivel;
	
	@Column(precision = 3, scale = 2)
	private Double preco;
	
	@Column(precision = 5, scale = 2)
	private Double valor;
	
	@Column(precision = 5, scale = 3)
	private Double litros;
	
	private Integer tempoConsumo;
	private LocalDate previsaoAbastecimento;
	
	@Column(name = "km_l", precision = 4, scale = 2)
	private Double kmL;
	private Boolean tanque;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public Integer getOdometro() {
		return odometro;
	}
	public void setOdometro(Integer odometro) {
		this.odometro = odometro;
	}
	public Integer getKmRodado() {
		return kmRodado;
	}
	public void setKmRodado(Integer kmRodado) {
		this.kmRodado = kmRodado;
	}
	public FuelEnum getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(FuelEnum combustivel) {
		this.combustivel = combustivel;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getLitros() {
		return litros;
	}
	public void setLitros(Double litros) {
		this.litros = litros;
	}
	public Integer getTempoConsumo() {
		return tempoConsumo;
	}
	public void setTempoConsumo(Integer tempoConsumo) {
		this.tempoConsumo = tempoConsumo;
	}
	public LocalDate getPrevisaoAbastecimento() {
		return previsaoAbastecimento;
	}
	public void setPrevisaoAbastecimento(LocalDate previsaoAbastecimento) {
		this.previsaoAbastecimento = previsaoAbastecimento;
	}
	public Double getKmL() {
		return kmL;
	}
	public void setKmL(Double kmL) {
		this.kmL = kmL;
	}
	public Boolean getTanque() {
		return tanque;
	}
	public void setTanque(Boolean tanque) {
		this.tanque = tanque;
	}
}
