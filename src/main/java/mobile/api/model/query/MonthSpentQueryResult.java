package mobile.api.model.query;

import java.io.Serializable;

public class MonthSpentQueryResult implements Serializable {

	private static final long serialVersionUID = 1L;
	private Object month_year;
	private Double month_sum;
	
	public MonthSpentQueryResult(Object month_year, Double month_sum) {
		this.month_year = month_year;
		this.month_sum = month_sum;
	}

	public Object getMonth_year() {
		return month_year;
	}

	public void setMonth_year(Object month_year) {
		this.month_year = month_year;
	}

	public Double getMonth_sum() {
		return month_sum;
	}

	public void setMonth_sum(Double month_sum) {
		this.month_sum = month_sum;
	}
}
