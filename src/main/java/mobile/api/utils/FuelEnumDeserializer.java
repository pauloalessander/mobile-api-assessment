package mobile.api.utils;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mobile.api.model.FuelEnum;

@Component
public class FuelEnumDeserializer implements Converter<String, FuelEnum> {

	@Override
	public FuelEnum convert(String value) {
        try {
			return FuelEnum.of(value);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
}
