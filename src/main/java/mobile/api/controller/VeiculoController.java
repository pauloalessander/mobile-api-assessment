package mobile.api.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import mobile.api.model.Veiculo;
import mobile.api.model.handling.VeiculoHandling;
import mobile.api.service.VeiculoService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/veiculo")
public class VeiculoController {

	@Autowired
	VeiculoService veiculoService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Veiculo>> list() {
		List<Veiculo> retorno;
		retorno = veiculoService.list();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> search(@PathVariable Integer id) {
		Veiculo retorno;
		retorno = veiculoService.findById(id);
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody VeiculoHandling veiculoHandling) {
		Veiculo obj = veiculoService.fromHandling(veiculoHandling);
		obj = veiculoService.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/listaCombustiveis", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> listCombustivel() {
		String retorno;
		retorno = veiculoService.listFuelEnums();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/gastosMensais", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> listMonthSpent() {
		String retorno;
		retorno = veiculoService.listMonthSpent();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/gastoAlcool", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> spentAlcohol() {
		String retorno;
		retorno = veiculoService.spentAlcohol();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/gastoGasolina", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> spentGasoline() {
		String retorno;
		retorno = veiculoService.spentGasoline();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/gastoDiesel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> spentDiesel() {
		String retorno;
		retorno = veiculoService.spentDiesel();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/gastoGas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> spentGas() {
		String retorno;
		retorno = veiculoService.spentGas();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaAlcool", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmWithAlcohol() {
		String retorno;
		retorno = veiculoService.kmWithAlcohol();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaGasolina", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmWithGasoline() {
		String retorno;
		retorno = veiculoService.kmWithGasoline();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaGas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmWithGas() {
		String retorno;
		retorno = veiculoService.kmWithGas();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaDiesel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmWithDiesel() {
		String retorno;
		retorno = veiculoService.kmWithDiesel();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaPorLitrosAlcool", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmLWithAlcohol() {
		String retorno;
		retorno = veiculoService.kmLWithAlcohol();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaPorLitrosDiesel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmLWithDiesel() {
		String retorno;
		retorno = veiculoService.kmLWithDiesel();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaPorLitrosGasolin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmLWithGasoline() {
		String retorno;
		retorno = veiculoService.kmLWithGasoline();
		return ResponseEntity.ok().body(retorno);
	}
	
	@RequestMapping(value = "/distanciaPorLitrosGas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> kmLWithGas() {
		String retorno;
		retorno = veiculoService.kmLWithGas();
		return ResponseEntity.ok().body(retorno);
	}
}
