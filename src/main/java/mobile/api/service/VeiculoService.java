package mobile.api.service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
//import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mobile.api.model.FuelEnum;
import mobile.api.model.Veiculo;
import mobile.api.model.handling.VeiculoHandling;
import mobile.api.model.query.MonthSpentQueryResult;
import mobile.api.repository.VeiculoRepository;

@Service
public class VeiculoService {

	@Autowired
	VeiculoRepository veiculoRepository;
	
	public List<Veiculo> list() {
		return veiculoRepository.findAll();
	}
	
	public String listMonthSpent() {
		List<MonthSpentQueryResult> lista = veiculoRepository.listMonthSpent();
		Map<String, List<MonthSpentQueryResult>> dados = new HashMap<String, List<MonthSpentQueryResult>>();
		dados.put("data", lista);
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String spentAlcohol() {
		List<Double> lista = veiculoRepository.spentAlcohol();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String spentDiesel() {
		List<Double> lista = veiculoRepository.spentDiesel();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String spentGasoline() {
		List<Double> lista = veiculoRepository.spentGasoline();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String spentGas() {
		List<Double> lista = veiculoRepository.spentGas();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmWithAlcohol() {
		List<Integer> lista = veiculoRepository.kmWithAlcohol();
		Map<String, Integer> dados = new HashMap<String, Integer>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmWithGasoline() {
		List<Integer> lista = veiculoRepository.kmWithGasoline();
		Map<String, Integer> dados = new HashMap<String, Integer>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmWithGas() {
		List<Integer> lista = veiculoRepository.kmWithGas();
		Map<String, Integer> dados = new HashMap<String, Integer>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmWithDiesel() {
		List<Integer> lista = veiculoRepository.kmWithDiesel();
		Map<String, Integer> dados = new HashMap<String, Integer>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmLWithAlcohol() {
		List<Double> lista = veiculoRepository.kmLWithAlcohol();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmLWithDiesel() {
		List<Double> lista = veiculoRepository.kmLWithDiesel();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmLWithGasoline() {
		List<Double> lista = veiculoRepository.kmLWithGasoline();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String kmLWithGas() {
		List<Double> lista = veiculoRepository.kmLWithGas();
		Map<String, Double> dados = new HashMap<String, Double>();
		dados.put("data", lista.get(0));
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}
	
	public String listFuelEnums() {
		List<String> lista = Stream.of(FuelEnum.values())
								.map(FuelEnum::name)
								.collect(Collectors.toList());
		Map<String, List<String>> dados = new HashMap<String, List<String>>();
		dados.put("data", lista);
		Gson gson = new GsonBuilder().create();
		return gson.toJson(dados);
	}

	public Veiculo fromHandling(VeiculoHandling veiculoHandling) {
		Veiculo toSave = new Veiculo();
		List<Veiculo> lista = veiculoRepository.findAllByOrderByIdAsc();
		
		Veiculo ultimo = lista.get(lista.size() - 1);
		Double average = veiculoRepository.averageTempoConsumo().get(0);
		Integer averageRound = (int) Math.round(average);
		
		Integer tempoConsumo = null;
		LocalDate previsaoAbastecimento = null;
		if (veiculoHandling.getData() != null) {
			Long period = ChronoUnit.DAYS.between(ultimo.getData(), veiculoHandling.getData());
			previsaoAbastecimento = veiculoHandling.getData().plusDays(averageRound.longValue());
			tempoConsumo = period.intValue();
		}
		
		Double litros = null;
		if (veiculoHandling.getValor() != null && veiculoHandling.getPreco() != null) {
			DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
			DecimalFormat decimalFormat = new DecimalFormat("0.00", symbols);
			Double div = veiculoHandling.getValor() / veiculoHandling.getPreco();
			litros = Double.valueOf(decimalFormat.format(div));
		}
		
		Integer kmRodado = null;
		if (veiculoHandling.getOdometro() != null) {
			kmRodado = veiculoHandling.getOdometro() - ultimo.getOdometro();
		}
		
		Double kmL = null;
		if (kmRodado != null && litros != null) {
			DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
			DecimalFormat decimalFormat = new DecimalFormat("0.00", symbols);
			Double div = kmRodado/litros;
			kmL = Double.valueOf(decimalFormat.format(div));
		}
		
		toSave.setData(veiculoHandling.getData());
		toSave.setHora(veiculoHandling.getHora());
		toSave.setOdometro(veiculoHandling.getOdometro());
		toSave.setKmRodado(kmRodado);
		toSave.setCombustivel(veiculoHandling.getCombustivel());
		toSave.setPreco(veiculoHandling.getPreco());
		toSave.setValor(veiculoHandling.getValor());
		toSave.setLitros(litros);
		toSave.setTempoConsumo(tempoConsumo);
		toSave.setPrevisaoAbastecimento(previsaoAbastecimento);
		toSave.setKmL(kmL);
		toSave.setTanque(veiculoHandling.getTanque());
		
		return toSave;
	}

	public Veiculo insert(Veiculo obj) {
		veiculoRepository.save(obj);
		List<Veiculo> lista = veiculoRepository.findAllByOrderByIdAsc();
		Veiculo obj2 = lista.get(lista.size() - 1); 
		return obj2;
	}
	
	public Veiculo findById(Integer id) {
		Optional<Veiculo> optional = veiculoRepository.findById(id);
		if(optional.isEmpty())
			return null;
		return optional.get();
	}
}
