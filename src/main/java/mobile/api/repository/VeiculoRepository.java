package mobile.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mobile.api.model.Veiculo;
import mobile.api.model.query.MonthSpentQueryResult;

public interface VeiculoRepository extends JpaRepository<Veiculo, Integer> {

	@Query("SELECT new mobile.api.model.query.MonthSpentQueryResult(date_format(data, '%M %Y') as month_year, sum(preco) as month_sum) FROM Veiculo "
			+ "GROUP BY year(data), month(data)")
	List<MonthSpentQueryResult> listMonthSpent();
	
	@Query("SELECT sum(v.preco) FROM Veiculo v "
			+ "WHERE v.combustivel = 'ETANOL'")
	List<Double> spentAlcohol();
	
	@Query("SELECT sum(v.preco) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GASOLINA'")
	List<Double> spentGasoline();
	
	@Query("SELECT sum(v.preco) FROM Veiculo v "
			+ "WHERE v.combustivel = 'DIESEL'")
	List<Double> spentDiesel();
	
	@Query("SELECT sum(v.preco) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GNV'")
	List<Double> spentGas();
	
	@Query("SELECT sum(v.kmRodado) FROM Veiculo v "
			+ "WHERE v.combustivel = 'ETANOL'")
	List<Integer> kmWithAlcohol();
	
	@Query("SELECT sum(v.kmRodado) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GASOLINA'")
	List<Integer> kmWithGasoline();
	
	@Query("SELECT sum(v.kmRodado) FROM Veiculo v "
			+ "WHERE v.combustivel = 'DIESEL'")
	List<Integer> kmWithDiesel();
	
	@Query("SELECT sum(v.kmRodado) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GNV'")
	List<Integer> kmWithGas();
	
	@Query("SELECT avg(v.kmL) FROM Veiculo v "
			+ "WHERE v.combustivel = 'ETANOL'")
	List<Double> kmLWithAlcohol();
	
	@Query("SELECT avg(v.kmL) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GASOLINA'")
	List<Double> kmLWithGasoline();
	
	@Query("SELECT avg(v.kmL) FROM Veiculo v "
			+ "WHERE v.combustivel = 'GNV'")
	List<Double> kmLWithGas();
	
	@Query("SELECT avg(v.kmL) FROM Veiculo v "
			+ "WHERE v.combustivel = 'DIESEL'")
	List<Double> kmLWithDiesel();
	
	@Query("SELECT avg(v.tempoConsumo) FROM Veiculo v")
	List<Double> averageTempoConsumo();
	
	List<Veiculo> findAllByOrderByIdAsc();
	
}
